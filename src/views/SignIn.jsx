import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { signIn } from "../firebase/Authentication";
import Alert from "../components/Alert";
import { Button, CssBaseline, TextField, FormControlLabel, Checkbox, Grid, Typography, Container, Paper, Link } from "@material-ui/core";
import useStyles from "../styles/css";

function SignIn() {
 const css = useStyles();
 const history = useHistory();
 const [user, setUser] = useState([]);
 const [alertErrorAuth, setAlertErrorAuth] = useState(false);
 const [messageError, setMessageError] = useState("");

 function pushHome() {
  history.push("/");
 }

 function handleSubmit(e) {
  e.preventDefault();
  signIn(user, pushHome, setAlertErrorAuth, setMessageError);
 }

 return (
  <Container component="main" maxWidth="xs">
   <Paper>
    <CssBaseline />

    <div className={css.paper}>
     <Typography component="h1" variant="h5" color="primary">
      Sign In
     </Typography>

     <form className={css.form} onSubmit={handleSubmit}>
      <TextField
       id="signInEmail"
       name="email"
       label="Email Address"
       autoComplete="email"
       type="email"
       margin="normal"
       variant="outlined"
       fullWidth
       required
       autoFocus
       onChange={(event) => {
        setUser((state) => ({
         ...state,
         email: event.target.value,
        }));
       }}
      />

      {alertErrorAuth ? <Alert state={setAlertErrorAuth} messageError={messageError} /> : null}

      <TextField
       id="signInPassword"
       name="password"
       label="Password"
       autoComplete="current-password"
       type="password"
       margin="normal"
       variant="outlined"
       required
       fullWidth
       onChange={(event) => {
        setUser((state) => ({
         ...state,
         password: event.target.value,
        }));
       }}
      />

      <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />

      <Button className={css.submit} color="primary" type="submit" variant="contained" fullWidth>
       Sign In
      </Button>

      <Grid container>
       <Grid item xs>
        <Link
         style={{ cursor: "pointer" }}
         onClick={() => {
          history.push("/");
         }}>
         Forgot password?
        </Link>
       </Grid>

       <Grid item>
        <Link
         style={{ cursor: "pointer" }}
         onClick={() => {
          history.push("/SignUp");
         }}>
         Don't have an account? Sign Up
        </Link>
       </Grid>
      </Grid>
     </form>
    </div>
   </Paper>
  </Container>
 );
}

export default SignIn;
