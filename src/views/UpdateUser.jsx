import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { auth, db } from "../firebase/Setting";
import { UpdateUser as Update } from "../data/CRUD";
import { DeleteUser } from "../data/CRUD";
import { Avatar, Button, CssBaseline, TextField, Grid, Typography, Container, Paper } from "@material-ui/core";
import useStyles from "../styles/css";

function UpdateUser() {
 const css = useStyles();
 const [user, setUser] = useState([]);
 const [stateUser, setStateUser] = useState();
 const [srcProfilePicture, setSrcProfilePicture] = useState(user.profilePicture);
 const [imgBoolean, setImgBoolean] = useState(false);
 const [fileUpload, setFileUpload] = useState();

 let history = useHistory();

 function pushViewsUsers() {
  history.push("/");
 }

 function handleSubmit(event) {
  event.preventDefault();
  Update(user, user.email, fileUpload, pushViewsUsers);
 }

 useEffect(() => {
  auth.onAuthStateChanged((userCurrent) => {
   /* GET USER */
   db
    .collection("users")
    .doc(userCurrent.email)
    .get()
    .then((doc) => {
     if (doc.exists) {
      setUser(doc.data());
      setStateUser(userCurrent);
      if (!imgBoolean) {
       setSrcProfilePicture(user.profilePicture);
      }
     } else {
      setUser("");
     }
    });
  });
 }, [imgBoolean, user.profilePicture]);

 return (
  <Container component="main" maxWidth="xs">
   <CssBaseline />

   <div className={css.paper}>
    <Avatar alt="Profile" src={srcProfilePicture} className={css.avatar} />
    {console.log("")}
    {/* <FileUpload userID={user.email} /> */}
    <Button variant="contained" component="label" color="primary">
     Upload File
     <input
      type="file"
      hidden
      onChange={(e) => {
       let file = e.target.files[0];
       setImgBoolean(true);
       setFileUpload(file);
       let tmppath = URL.createObjectURL(file);
       setSrcProfilePicture(tmppath);
      }}
     />
    </Button>
    <br />

    <Typography component="h1" variant="h5">
     Update User
    </Typography>

    {stateUser ? (
     <form className={css.form} onSubmit={handleSubmit}>
      <Grid container spacing={2}>
       <Grid item xs={12}>
        <Paper>
         <TextField
          id="signUpUserName"
          name="userName"
          label="User Name"
          autoComplete="username"
          defaultValue={user.username}
          variant="outlined"
          fullWidth
          required
          autoFocus
          onChange={(event) => {
           setUser((state) => ({
            ...state,
            username: event.target.value,
           }));
          }}
         />
        </Paper>
       </Grid>

       <Grid item xs={12} sm={6}>
        <Paper>
         <TextField
          id="signUpName"
          name="firstName"
          label="First Name"
          autoComplete="First name"
          defaultValue={user.name}
          variant="outlined"
          fullWidth
          required
          onChange={(event) => {
           event.target.focus();
           setUser((state) => ({
            ...state,
            name: event.target.value,
           }));
          }}
         />
        </Paper>
       </Grid>

       <Grid item xs={12} sm={6}>
        <Paper>
         <TextField
          id="signUpLastName"
          name="lastname"
          label="Last Name"
          autoComplete="lastname"
          defaultValue={user.lastname}
          variant="outlined"
          fullWidth
          required
          onChange={(event) => {
           setUser((state) => ({
            ...state,
            lastname: event.target.value,
           }));
          }}
         />
        </Paper>
       </Grid>

       <Grid item xs={12}>
        <Paper>
         <TextField
          id="signUpEmail"
          name="email"
          label="Email Address"
          autoComplete="email"
          defaultValue={user.email}
          type="email"
          variant="outlined"
          fullWidth
          onChange={(event) => {
           setUser((state) => ({
            ...state,
            email: event.target.value,
           }));
          }}
         />
        </Paper>
       </Grid>

       <Grid item xs={12}>
        <Paper>
         <TextField
          id="signUpPassword"
          name="password"
          label="Password"
          autoComplete="password"
          defaultValue={user.password}
          variant="outlined"
          type="password"
          fullWidth
          required
          onChange={(event) => {
           setUser((state) => ({
            ...state,
            password: event.target.value,
           }));
          }}
         />
        </Paper>
       </Grid>
      </Grid>

      <Button type="submit" variant="contained" color="primary" className={css.submit} fullWidth>
       Update
      </Button>

      <Button
       style={{ background: "red", color: "white" }}
       type="submit"
       variant="contained"
       fullWidth
       onClick={() => {
        history.push("/");
       }}>
       Cancel
      </Button>

      <Button
       style={{ marginTop: "20px" }}
       variant="outlined"
       color="secondary"
       fullWidth
       onClick={() => {
        if (window.confirm("Esta seguro de eliminar su cuenta?")) {
         DeleteUser(user.email, pushViewsUsers);
        }
       }}>
       Delete Account
      </Button>
     </form>
    ) : (
     <h1>"LOADING..."</h1>
    )}
   </div>
  </Container>
 );
}

export default UpdateUser;
