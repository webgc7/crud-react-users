import React from "react";
import ReactDOM from "react-dom";
import Router from "./components/Router";
import "./styles/index.css";

ReactDOM.render(
 <React.StrictMode>
  <Router />
 </React.StrictMode>,
 document.getElementById("root")
);
