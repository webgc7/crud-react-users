import { useHistory } from "react-router-dom";
import { Box, Fab } from "@material-ui/core";
import { LinkedIn, Email, Copyright } from "@material-ui/icons";
import useStyles from "../styles/css";

function Footer() {
 const css = useStyles();
 let history = useHistory();

 return (
  <Box display="flex" justifyContent="center" style={{ marginTop: "10px" }}>
   <Fab title="Linkedin" style={{ background: "#0077b5", color: "white" }} href="https://linkedin.com/in/webgc/" target="_black" className={css.Fab}>
    <LinkedIn fontSize="large" />
   </Fab>

   <Fab
    title="Gmail"
    style={{ background: "Red", color: "white" }}
    className={css.Fab}
    onClick={() => {
     alert("Gmail: webgc7@gmail.com");
    }}>
    <Email fontSize="large" />
   </Fab>

   <Fab
    title="Copyrigth"
    style={{ background: "#0077b5", color: "white" }}
    className={css.Fab}
    onClick={() => {
     history.push("/Copyrigth");
    }}>
    <Copyright fontSize="large" />
   </Fab>
  </Box>
 );
}

export default Footer;
