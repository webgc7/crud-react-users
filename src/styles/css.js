import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
 paper: {
  marginTop: theme.spacing(3),
  padding: theme.spacing(1),
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
 },

 avatar: { margin: theme.spacing(1), width: theme.spacing(15), height: theme.spacing(15) },
 avatarUsers: { marginRight: theme.spacing(1), width: theme.spacing(10), height: theme.spacing(10) },

 form: {
  width: "100%",
  marginTop: theme.spacing(1),
 },

 submit: {
  margin: theme.spacing(2, 0),
 },

 listUsers: { width: "100%" },

 icons: { margin: "" },

 Fab: { margin: 10 },

 textAlignCenter: { textAlign: "center" },
}));

export default useStyles;
