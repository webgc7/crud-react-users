import { auth } from "./Setting";

export function signIn(user, pushHome, setAlertErrorAuth, setMessageError) {
 auth
  .signInWithEmailAndPassword(user.email, user.password)
  .then((userCredential) => {
   pushHome();
  })
  .catch((error) => {
   setMessageError(error.message);
   setAlertErrorAuth(true);
  });
}

export function signOut(signOutPush) {
 auth
  .signOut()
  .then(() => {
   signOutPush();
  })
  .catch((error) => {
   console.log(error);
  });
}
